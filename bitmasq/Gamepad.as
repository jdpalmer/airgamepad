package bitmasq {

  import flash.display.Stage;
  import flash.events.Event;
  import flash.events.EventDispatcher;
  import flash.events.GameInputEvent;
  import flash.events.KeyboardEvent;
  import flash.ui.GameInput;
  import flash.ui.GameInputControl;
  import flash.ui.GameInputDevice;
  import flash.ui.Keyboard;
  import flash.system.Capabilities;
  import flash.utils.Dictionary;

  public class Gamepad {

    public static const LSTICK:Number = 1;
    public static const LSTICK_X:Number = 2;
    public static const LSTICK_Y:Number = 3;

    public static const RSTICK:Number = 4;
    public static const RSTICK_X:Number = 5;
    public static const RSTICK_Y:Number = 6;

    public static const D_UP:Number = 7;
    public static const D_DOWN:Number = 8;
    public static const D_LEFT:Number = 9;
    public static const D_RIGHT:Number = 10;

    public static const A_UP:Number = 11;
    public static const A_DOWN:Number = 12;
    public static const A_LEFT:Number = 13;
    public static const A_RIGHT:Number = 14;

    public static const LB:Number = 15;
    public static const RB:Number = 16;

    public static const LT:Number = 17;
    public static const LT_X:Number = 18;

    public static const RT:Number = 19;
    public static const RT_X:Number = 20;

    public static const MENU:Number = 21;

    // Ouya Aliases
    public static const BUTTON_O:Number = A_DOWN;
    public static const BUTTON_U:Number = A_LEFT;
    public static const BUTTON_Y:Number = A_UP;
    public static const BUTTON_A:Number = A_RIGHT;

    // I use the following abbreviations below:
    //
    // A2DP - Convert 0..1 to 0/1
    // A2DN - Convert -1..0 to 0/1
    // D2AP - Convert 0/1 to 0..1
    // D2AN - Convert 0/1 to 0..-1

    private static const profiles:Object = {

      'ouya/ouya': {

        // Tested platforms:
        // "Ouya"

        // Tested device strings:
        // "OUYA Game Controller"

        // Issues:
        // OUYA button is mapped to Keyboard.MENU (ambiguous)

        name: 'ouya/ouya',

        AXIS_0: LSTICK_X,
        AXIS_1: LSTICK_Y,
        BUTTON_106: LSTICK,

        AXIS_11: RSTICK_X,
        AXIS_14: RSTICK_Y,
        BUTTON_107: RSTICK,

        BUTTON_19: D_UP,
        BUTTON_20: D_DOWN,
        BUTTON_21: D_LEFT,
        BUTTON_22: D_RIGHT,

        BUTTON_96: A_DOWN,
        BUTTON_99: A_LEFT,
        BUTTON_100: A_UP,
        BUTTON_97: A_RIGHT,

        BUTTON_102: LB,
        BUTTON_103: RB,

        AXIS_17: LT_X,
        BUTTON_104: LT,

        AXIS_18: RT_X,
        BUTTON_105: RT,

        MUL: {},
        A2DP: {},
        A2DN: {},
        D2AP: {},
        D2AN: {}

      },

      'ouya/ps3': {

        // Tested platforms:
        // "Ouya"

        // Tested device strings:
        // "Sony PLAYSTATION(R)3 Controller"

        // Issues:
        // START is mapped to MENU (not ambiguous) and Keyboard.MENU (ambiguous)
        // PS is mapped to Keyboard.MENU (ambiguous)

        name: 'ouya/ps3',

        AXIS_0: LSTICK_X,
        AXIS_1: LSTICK_Y,
        BUTTON_106: LSTICK,

        AXIS_11: RSTICK_X,
        AXIS_14: RSTICK_Y,
        BUTTON_107: RSTICK,

        BUTTON_19: D_UP,
        BUTTON_20: D_DOWN,
        BUTTON_21: D_LEFT,
        BUTTON_22: D_RIGHT,

        BUTTON_96: A_DOWN,
        BUTTON_99: A_LEFT,
        BUTTON_100: A_UP,
        BUTTON_97: A_RIGHT,

        BUTTON_102: LB,
        BUTTON_103: RB,

        AXIS_17: LT_X,
        BUTTON_104: LT,

        AXIS_18: RT_X,
        BUTTON_105: RT,

        BUTTON_108: MENU, // START

        MUL: {},
        A2DP: {},
        A2DN: {},
        D2AP: {},
        D2AN: {}

      },

      'ouya/xbox360': {

        // Tested platforms:
        // "Ouya"

        // Tested Device Strings:
        // "Rock Candy Gamepad for Xbox 360 (Wired)"
        // "Microsoft X-Box 360 pad"

        // Issues:
        // START is mapped to MENU (not ambiguous) and Keyboard.MENU (ambiguous)
        // Xbox button is mapped to Keyboard.MENU (ambiguous)

        name: 'ouya/xbox360',

        AXIS_0: LSTICK_X,
        AXIS_1: LSTICK_Y,
        BUTTON_106: LSTICK,

        AXIS_11: RSTICK_X,
        AXIS_14: RSTICK_Y,
        BUTTON_107: RSTICK,

        BUTTON_19: D_UP,
        BUTTON_20: D_DOWN,
        BUTTON_21: D_LEFT,
        BUTTON_22: D_RIGHT,

        BUTTON_96: A_DOWN,
        BUTTON_99: A_LEFT,
        BUTTON_100: A_UP,
        BUTTON_97: A_RIGHT,

        BUTTON_102: LB,
        BUTTON_103: RB,

        AXIS_17: LT_X,
        AXIS_18: RT_X,

        BUTTON_108: MENU, // START

        // The Xbox D-Pad is pressure sensitive but we don't
        // use these mappings:
        // AXIS_15: D_PAD_X
        // AXIS_16: D_PAD_Y

        MUL: {},
        A2DP: {
          AXIS_2: LT, // 17
          AXIS_5: RT // 18
        },
        A2DN: {
        },
        D2AP: {},
        D2AN: {}

      },

      'ouya/xbox': {

        // Tested platforms:
        // "Ouya"

        // Tested Device Strings:
        // Generic X-Box pad

        // Issues:
        // START is mapped to MENU (not ambiguous) and Keyboard.MENU (ambiguous)
        // Xbox button is mapped to Keyboard.MENU (ambiguous)

        name: 'ouya/xbox',

        AXIS_0: LSTICK_X,
        AXIS_1: LSTICK_Y,
        BUTTON_106: LSTICK,

        AXIS_11: RSTICK_X,
        AXIS_14: RSTICK_Y,
        BUTTON_107: RSTICK,

/*        BUTTON_19: D_UP,
        BUTTON_20: D_DOWN,
        BUTTON_21: D_LEFT,
        BUTTON_22: D_RIGHT, */

        BUTTON_96: A_DOWN,
        BUTTON_99: A_LEFT,
        BUTTON_100: A_UP,
        BUTTON_97: A_RIGHT,

        BUTTON_102: LB,
        BUTTON_103: RB,

        AXIS_17: LT_X,
        AXIS_18: RT_X,

        BUTTON_108: MENU, // START

        // The Xbox D-Pad is pressure sensitive but we don't
        // use these mappings:
        // AXIS_15: D_PAD_X
        // AXIS_16: D_PAD_Y

        MUL: {},
        A2DP: {
          //AXIS_2: LT,
          //AXIS_5: RT,
          AXIS_17: LT,
          AXIS_18: RT,
          AXIS_15: D_RIGHT,
          AXIS_16: D_DOWN,
          AXIS_0: D_RIGHT,
          AXIS_1: D_DOWN
        },
        A2DN: {
          AXIS_15: D_LEFT,
          AXIS_16: D_UP,
          AXIS_0: D_LEFT,
          AXIS_1: D_UP
        },
        D2AP: {},
        D2AN: {}

      },

      'windows/ps4': {

        // Tested platforms:
        // "Windows 7"
        // "Windows 8"

        // Tested device strings:
        // "Rock Candy Gamepad for Xbox 360 (Wired)"
        // "Microsoft X-Box 360 pad"

        // Issues:
        // No access to XBox button.
        // We map BACK and START to MENU.

        name: 'windows/ps4',

        AXIS_0: LSTICK_X,
        AXIS_1: LSTICK_Y,
        BUTTON_20: LSTICK,

        AXIS_2: RSTICK_X,
        AXIS_5: RSTICK_Y,
        BUTTON_21: RSTICK,

        BUTTON_6: D_UP,
        BUTTON_7: D_DOWN,
        BUTTON_8: D_LEFT,
        BUTTON_9: D_RIGHT,

        BUTTON_11: A_DOWN,
        BUTTON_10: A_LEFT,
        BUTTON_13: A_UP,
        BUTTON_12: A_RIGHT,

        BUTTON_14: LB,
        BUTTON_15: RB,

        BUTTON_18: MENU, // SHARE
        BUTTON_19: MENU, // OPTIONS
        BUTTON_22: MENU, // PS BUTTON
        // BUTTON_23: IGNORED - TRACKPAD PRESS

        AXIS_3: LT_X,
        BUTTON_16: LT,

        AXIS_4: RT_X,
        BUTTON_17: RT,

        MUL: {
          AXIS_1: -1,
          AXIS_5: -1
        },
        A2DP: {
          AXIS_0: D_RIGHT,
          AXIS_1: D_DOWN
        },
        A2DN: {
          AXIS_0: D_LEFT,
          AXIS_1: D_UP
        },
        D2AP: {},
        D2AN: {}

      },

      'windows/xbox360': {

        // Tested platforms:
        // "Windows 7"
        // "Windows 8"

        // Tested device strings:
        // "Rock Candy Gamepad for Xbox 360 (Wired)"
        // "Microsoft X-Box 360 pad"

        // Issues:
        // No access to XBox button.
        // We map BACK and START to MENU.

        name: 'windows/xbox360',

        AXIS_0: LSTICK_X,
        AXIS_1: LSTICK_Y,
        BUTTON_14: LSTICK,

        AXIS_2: RSTICK_X,
        AXIS_3: RSTICK_Y,
        BUTTON_15: RSTICK,

        BUTTON_16: D_UP,
        BUTTON_17: D_DOWN,
        BUTTON_18: D_LEFT,
        BUTTON_19: D_RIGHT,

        BUTTON_4: A_DOWN,
        BUTTON_6: A_LEFT,
        BUTTON_7: A_UP,
        BUTTON_5: A_RIGHT,

        BUTTON_8: LB,
        BUTTON_9: RB,

        BUTTON_12: MENU, // BACK
        BUTTON_13: MENU, // START

        BUTTON_10: LT_X,
        BUTTON_11: RT_X,

        MUL: {
          AXIS_1: -1,
          AXIS_3: -1
        },
        A2DP: {
          BUTTON_10: LT,
          BUTTON_11: RT,
          AXIS_0: D_RIGHT,
          AXIS_1: D_DOWN
        },
        A2DN: {
          AXIS_0: D_LEFT,
          AXIS_1: D_UP
        },
        D2AP: {},
        D2AN: {}

      },

      'mac/xbox360': {

        // Tested platforms:
        // "OS X 10.8.4 / tattiebogle driver"

        // Tested Device Strings:
        // "Rock Candy Gamepad for Xbox 360 (Wired)"

        // Issues:
        // We map BACK and START to MENU.

        name: 'mac/xbox360',

        AXIS_0: LSTICK_X,
        AXIS_1: LSTICK_Y,
        BUTTON_12: LSTICK,

        AXIS_3: RSTICK_X,
        AXIS_4: RSTICK_Y,
        BUTTON_13: RSTICK,

        BUTTON_6: D_UP,
        BUTTON_7: D_DOWN,
        BUTTON_8: D_LEFT,
        BUTTON_9: D_RIGHT,

        BUTTON_17: A_DOWN,
        BUTTON_19: A_LEFT,
        BUTTON_20: A_UP,
        BUTTON_18: A_RIGHT,

        BUTTON_14: LB,
        BUTTON_15: RB,

        AXIS_2: LT_X,
        AXIS_5: RT_X,

        BUTTON_11: MENU, // BACK
        BUTTON_10: MENU, // START

        BUTTON_16: MENU, // XBOX

        MUL: {},
        A2DP: {
          AXIS_2: LT,
          AXIS_5: RT,
          AXIS_0: D_RIGHT,
          AXIS_1: D_DOWN
        },
        A2DN: {
          AXIS_0: D_LEFT,
          AXIS_1: D_UP
        },
        D2AP: {},
        D2AN: {}

      },

      'mac/ps3': {

        // Tested platforms:
        // "OS X 10.8.4 / tattiebogle driver"

        // Tested Device Strings:
        // "Rock Candy Gamepad for Xbox 360 (Wired)"

        // Issues:
        // We map SELECT and START to MENU.

        name: 'mac/ps3',

        AXIS_0: LSTICK_X,
        AXIS_1: LSTICK_Y,
        BUTTON_5: LSTICK,

        AXIS_2: RSTICK_X,
        AXIS_3: RSTICK_Y,
        BUTTON_6: RSTICK,

        BUTTON_8: D_UP,
        BUTTON_10: D_DOWN,
        BUTTON_11: D_LEFT,
        BUTTON_9: D_RIGHT,

        BUTTON_18: A_DOWN,
        BUTTON_19: A_LEFT,
        BUTTON_16: A_UP,
        BUTTON_17: A_RIGHT,

        BUTTON_14: LB,
        BUTTON_15: RB,

        BUTTON_12: LT,
        BUTTON_13: RT,

        BUTTON_4: MENU, // SELECT
        BUTTON_7: MENU, // START
        BUTTON_20: MENU, // PS

        MUL: {},
        A2DP: {
          AXIS_0: D_RIGHT,
          AXIS_1: D_DOWN
        },
        A2DN: {
          AXIS_0: D_LEFT,
          AXIS_1: D_UP
        },
        D2AP: {
          BUTTON_12: LT_X,
          BUTTON_13: RT_X
        },
        D2AN: {}

      },

      'windowsxp/xbox360': {

        // Tested platforms:
        // "Windows XP"

        // Tested device strings:
        // "Rock Candy Gamepad for Xbox 360 (Wired)"

        // Issues:
        // No access to XBOX button.
        // LT,RT,LT_X,RT_X is ambiguous - all linked to AXIS_2.
        // We map BACK and START to MENU.

        name: 'windowsxp/xbox360',

        AXIS_0: LSTICK_X,
        AXIS_1: LSTICK_Y,
        BUTTON_17: LSTICK,

        AXIS_3: RSTICK_X,
        AXIS_4: RSTICK_Y,
        BUTTON_18: RSTICK,

        BUTTON_5: D_UP,
        BUTTON_6: D_DOWN,
        BUTTON_7: D_LEFT,
        BUTTON_8: D_RIGHT,

        BUTTON_9: A_DOWN,
        BUTTON_11: A_LEFT,
        BUTTON_12: A_UP,
        BUTTON_10: A_RIGHT,

        BUTTON_13: LB,
        BUTTON_14: RB,

        BUTTON_15: MENU,
        BUTTON_16: MENU,

        MUL: {},
        A2DP: {
          AXIS_0: D_RIGHT,
          AXIS_1: D_DOWN,
          AXIS_2: LT
        },
        A2DN: {
          AXIS_0: D_LEFT,
          AXIS_1: D_UP,
          AXIS_2: RT
        },
        D2AP: {},
        D2AN: {}

      }
    };

    public static var traceFunction:Function = null;
    public static var inspectFunction:Function = null;
    public static var deadZone:Number = 0.11;

    private static var gameInput:GameInput;

    // Stores profiles based on device keys.
    private static var profileCache:Dictionary = new Dictionary();

    // The emulationCache keeps track of the last GameInput value for
    // a device-GameInput control combination.  This information is
    // used to decide if new events should be dispatched.
    private static var emulationCache:Dictionary = new Dictionary();

    // The stateCache keeps track of the last dispatched value for a
    // device-Gamepad control combination.  This information is used
    // to query the state of the device.  The stateCache and
    // emulationCache do *not* have the same values - the stateCache
    // is the filtered final output of all Gamepad events while the
    // emulationCache has raw values for analog controls.
    private static var stateCache:Dictionary = new Dictionary();

    private static var devices:Array = [];
    private static var dispatchers:Array = [];

    private static var gamepad:Gamepad;
    private static var stage:Stage;

    function Gamepad() {

      if (gamepad) throw new Error("Gamepad has already been initialized. It may be fetched with Gamepad.get()");
      if (!stage) throw new Error("Gamepad must be initialized with Gamepad.init()");

      if (traceFunction != null)
        traceFunction("Gamepad() GameInput initialized");

      gameInput = new GameInput();
      gameInput.addEventListener(GameInputEvent.DEVICE_ADDED, handleDeviceAdded);
      gameInput.addEventListener(GameInputEvent.DEVICE_REMOVED, handleDeviceRemoved);

      dispatchers.push(new EventDispatcher());

      stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
      stage.addEventListener(KeyboardEvent.KEY_UP, keyUpHandler);

    }

    public static function init(_stage:Stage):Gamepad {

      stage = _stage;
      if (gamepad) throw new Error("Gamepad has already been initialized.");
      gamepad = new Gamepad();

      if (traceFunction != null)
        traceFunction("Gamepad initialized");

      return gamepad;

    }

    public static function get():Gamepad {

      if (!gamepad) throw new Error("Gamepad must first be initialized with Gamepad.init()");
      return gamepad;

    }

    public function addEventListener(ev:String, fn:Function):void {

      dispatchers[dispatchers.length-1].addEventListener(ev, fn);

    }

    public function removeEventListener(ev:String, fn:Function):void {
 
     dispatchers[dispatchers.length-1].removeEventListener(ev, fn);
 
   }

    public function dispatchEvent(type:String,
                                  controlCode:Number,
                                  value:Number,
                                  device:Object,
                                  deviceCode:Number):void {

      if (traceFunction != null)
        traceFunction("dispatchEvent");

      if (type == GamepadEvent.CHANGE) {
        if (!stateCache[deviceCode]) stateCache[deviceCode] = new Dictionary();
        stateCache[deviceCode][controlCode] = value;
      }

      if (traceFunction != null)
        traceFunction("dispatching..");

      dispatchers[dispatchers.length-1].
        dispatchEvent(new GamepadEvent(type,
                                       controlCode,
                                       value,
                                       device,
                                       deviceCode));

    }

    public function addKeyController(map:Dictionary):void {

      devices.push(map);
      dispatchEvent(GamepadEvent.DEVICE_ADDED,
                    0,
                    0,
                    null,
                    1);

    }

    public function pushContext():void {
      dispatchers.push(new EventDispatcher());
    }

    public function popContext():void {
      dispatchers.pop();
    }

    public function query(deviceCode:Number, controlCode:Number):Number {
      if (!stateCache[deviceCode]) {
        // This is an error.  Raise an exception?
        return 0;
      }
      if (stateCache[deviceCode][controlCode]) {
        return stateCache[deviceCode][controlCode];
      }
      return 0;
    }

    private function keyDownHandler(event:KeyboardEvent):void {

      if (event.keyCode == Keyboard.MENU) {

        // On Ouya, the MENU always gets fired from the first device
        // regardless of where it actually came from.  AIR doesn't
        // give us enough information to be usefully non-ambiguous.

        dispatchEvent(GamepadEvent.CHANGE,
                      MENU,
                      1,
                      devices[0],
                      0);
        return;
      }

      for (var index:Number=0; index < devices.length; index++) {

        if (devices[index] is GameInputDevice) continue;

        if (devices[index][event.keyCode]) {
          dispatchEvent(GamepadEvent.CHANGE,
                        devices[index][event.keyCode],
                        1,
                        devices[index],
                        index);
        }
      }
    }

    private function keyUpHandler(event:KeyboardEvent):void {

      if (event.keyCode == Keyboard.MENU) {
        dispatchEvent(GamepadEvent.CHANGE,
                      MENU,
                      0,
                      devices[0],
                      0);
        return;
      }

      for (var index:Number=0; index < devices.length; index++) {

        if (devices[index] is GameInputDevice) continue;

        if (devices[index][event.keyCode]) {
          dispatchEvent(GamepadEvent.CHANGE,
                        devices[index][event.keyCode],
                        0,
                        devices[index],
                        index);
        }
      }

    }

    private function handleDeviceAdded(event:GameInputEvent):void {

      if (traceFunction != null)
        traceFunction("GameInput detected 'device added'");

      var device_i:GameInputDevice;

      for (var i:Number = 0; i < GameInput.numDevices; i++) {
        device_i = GameInput.getDeviceAt(i);
        if (devices.indexOf(device_i) >= 0) continue;
        device_i.enabled = true;
        devices.push(device_i);
        for (var j:Number = 0; j < device_i.numControls; j++) {
          device_i.getControlAt(j).addEventListener(Event.CHANGE, handleDeviceChange);
        }
        break;
      }

      if (traceFunction != null)
        traceFunction("GameInput detected '" + device_i.name + "'");

      if (inspectFunction != null)
        inspectFunction(device_i);

      dispatchEvent(GamepadEvent.DEVICE_ADDED,
                    0,
                    0,
                    device_i,
                    devices.length - 1);

    }

    private function handleDeviceRemoved(event:GameInputEvent):void {

      if (traceFunction != null)
        traceFunction("GameInput detected 'device removed'");

      var device_i:GameInputDevice = null;
      var devices_new:Array = [];
      var i:Number;

      for (i = 0; i < GameInput.numDevices; i++) {
        devices_new.push(GameInput.getDeviceAt(i));
      }

      for (i = 0; i < devices.length; i++) {
        device_i = devices[i]
        if (!(device_i is GameInputDevice)) continue;
        if (devices_new.indexOf(device_i) < 0) {
          devices.splice(i, 1);
          break;
        }
      }

      dispatchEvent(GamepadEvent.DEVICE_REMOVED,
                    0,
                    0,
                    device_i,
                    i);

    }

    private function handleDeviceChange(event:Event):void {

      var control:GameInputControl = event.target as GameInputControl;
      var profile:Object = profileCache[control.device];

      if (traceFunction != null)
        traceFunction("GameInput detected 'device changed'");

      if (traceFunction != null)
        traceFunction("GameInput control (a) '" + control.id + "' fired change event with value, " + control.value);

      if (!profile) {

        // We default to an Ouya controller on an Ouya.  If we can't figure anything else out we will use this.
        profile = profiles['ouya/ouya'];

        var _platform:String = flash.system.Capabilities.version.substring(0, 3);

        if (control.device.name.indexOf("OUYA") >= 0) {

          // It is an Ouya controller!  We assume on an actual Ouya.
          profile = profiles['ouya/ouya'];


        } else if (control.device.name.indexOf("Generic X-Box pad") >= 0) {

          if (_platform == "AND") {
            profile = profiles['ouya/xbox'];
          }

        } else if (control.device.name.indexOf("360") >= 0) {

          // The possible platform values are: WIN, MAC, LNX, AND
          if (_platform == "AND") {
            profile = profiles['ouya/xbox360'];
          }

          if (_platform == "MAC") {
            profile = profiles['mac/xbox360'];
          }

          if (_platform == "WIN") {
            profile = profiles['windows/xbox360'];

            if (flash.system.Capabilities.os == "Windows XP" ||
                flash.system.Capabilities.os == "Windows XP 64") {
              profile = profiles['windowsxp/xbox360'];
            }
          }

        } else if (control.device.name.indexOf(")3") >= 0) {

          // TODO: PS3 controllers on MAC and WIN have not been tested..
          if (_platform == "AND") {
            profile = profiles['ouya/ps3'];
          }

          if (_platform == "MAC") {
            profile = profiles['mac/ps3'];
          }

        } else if (control.device.name == "Wireless Controller") {

          // TODO: PS4 controller untested
          if (_platform == "WIN") {
            profile = profiles['windows/ps4'];
          }

        }
        profileCache[control.device] = profile;
      }

      if (!profile) {
        throw new Error("Gamepad profile selection error.");
      }

      var deviceCode:Number = devices.indexOf(control.device);
      if (deviceCode == -1) {
        // The GameInput API wasn't honest!  Register an add event before proceeding.
        if (traceFunction != null)
          traceFunction("Creating fake GameInput 'device added'");
        handleDeviceAdded(null);
        deviceCode = devices.indexOf(control.device);
        if (deviceCode == -1) {
          throw new Error("Gamepad error determining device index.");
        }
      }

      var controlValue:Number = control.value;
      var controlId:String = control.id;

      if (profile['MUL'][controlId]) {
        controlValue = controlValue * profile['MUL'][controlId];
      }

      if (controlId.indexOf('AXIS') >= 0) {
        // Dead zone..
        var lastValue:Number = 0.0;
        var testValue:Number = controlValue < 0 ? -controlValue : controlValue;

        if (!emulationCache[control.device]) {
          emulationCache[control.device] = new Dictionary();
        }

        if (emulationCache[control.device][profile[controlId]]) {
          lastValue = emulationCache[control.device][profile[controlId]];
        }

        if (lastValue < deadZone && testValue < deadZone) {
          return;
        }

        if (testValue < deadZone) {
          controlValue = 0.0;
        }

        emulationCache[control.device][profile[controlId]] = testValue;
      }

      if (traceFunction != null)
        traceFunction("GameInput control (b) '" + controlId + "' fired change event with value, " + controlValue);

      if (profile[controlId]) {
        if (traceFunction != null)
          traceFunction("Dispatched GamepadEvent (direct).");

        dispatchEvent(GamepadEvent.CHANGE,
                      profile[controlId],
                      controlValue,
                      control.device,
                      deviceCode);
      }

      // Convert digital events to analog (j=0) and analog events to
      // digital (j=1) for negative (i=-1) and positive (i=1)
      // conversions that have been registered in each profile.
      for (var j:Number = 0; j <= 1; j += 1) {

        for (var i:Number = -1; i <= 1; i += 2) {

          var conversion:Object;
          if (j == 0) {
            if (i == -1) conversion = profile.A2DN;
            else conversion = profile.A2DP;
          } else {
            if (i == -1) conversion = profile.D2AN;
            else conversion = profile.D2AP;
          }

          if (conversion[controlId]) {

            var idealControl:Number = conversion[controlId];

            if (!emulationCache[control.device]) {
              emulationCache[control.device] = new Dictionary();
            }

            var newValue:Number = controlValue;
            var oldValue:Number = 0.0;

            if (j == 0) {
              newValue *= i;
            }

            if (emulationCache[control.device][idealControl]) {
              oldValue = emulationCache[control.device][idealControl];
            }

            if (oldValue < 0.5 && newValue >= 0.5) {
              if (traceFunction != null)
                traceFunction("Dispatched GamepadEvent (as 1).");

              dispatchEvent(GamepadEvent.CHANGE,
                            idealControl,
                            (j == 0) ? 1 : i,
                            control.device,
                            deviceCode);
            }

            if (oldValue >= 0.5 && newValue < 0.5) {
              if (traceFunction != null)
                traceFunction("Dispatched GamepadEvent (as 0).");

              dispatchEvent(GamepadEvent.CHANGE,
                            idealControl,
                            0,
                            control.device,
                            deviceCode);
            }
            emulationCache[control.device][idealControl] = newValue;
          }
        }
      }

    }
  }
}
