ifeq ($(OS),Windows_NT)
   FLEX_SDK=c:/FlexSDK
else
   FLEX_SDK=/opt/flex-sdk
endif

SOURCES=bitmasq/Gamepad.as bitmasq/GamepadEvent.as

ACOMPC=$(FLEX_SDK)/bin/acompc

all: gamepad.swc

clean:
	rm -rf gamepad.swc

gamepad.swc: $(SOURCES)
	$(ACOMPC) -source-path . -include-classes bitmasq.Gamepad bitmasq.GamepadEvent -output gamepad.swc

hx:
	haxe -swf-version 11.8 -lib air3 -swf mylib.swc -cp bitmasq Gamepad GamepadEvent
