I've only tested the SWC extensively using the command line build tools but I just did this very basic test and it seems to work.

Step 1.  Download the latest Adobe Air 3.8 or 3.9.  You can get the latest version here (within the section "SDK and Compiler"):

http://labs.adobe.com/downloads/air.html

Step 2. Add the SDK to Flash by going to Help > Manage Air SDK.  Click the + and then select the decompressed Air SDK folder.

Step 3. Go to File > Publish Settings.  Change the target to Air 3.x for Android where x needs to be 8 or 9.

Step 4. Put the Gamepad SWC in the same directory as your FLA.

Step 5. Go to File > Action Script Settings.  Click on the Library Path tab.  Then click on the little "F" icon with the hover tip "Browse to SWC file".  Select the Gamepad SWC.

Step 6. Now you can use Gamepad.  For example, on the first frame you can add:

     import bitmasq.Gamepad;
     import bitmasq.GamepadEvent;
     
     Gamepad.init(stage);
     Gamepad.get().addEventListener(GamepadEvent.CHANGE, onChange);
     
     function onChange(event:GamepadEvent):void {
       if (event.control == Gamepad.D_LEFT && event.value == 1) {
          trace("STEER LEFT!");
       }
     }

Step 7. Plugin a gamepad and test the movie.  You should see trace statements each time you press the left d-pad.
